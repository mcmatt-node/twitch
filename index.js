"use strict";

let log = require('log');
let normalise = require('normalise');
let tmi = require('tmi.js');

module.exports = function (options, credentials, channels) {
    let lastKnownServer = null;

    log.info('Creating IRC client.');

    options = Object.assign({
        channels: [],
        connection: {
            cluster: "aws",
            reconnect: true,
        },
        logger: log,
        identity: {
            username: "",
            password: "",
        },
    }, options, {identity: credentials}, {channels: channels || []});

    let client = new tmi.client(options);

    client.on('connecting', (address, port) => {
        log.info(`Connecting to ${address}:${port}...`);
    });
    client.on('connected', (address, port) => {
        lastKnownServer = `${address}:${port}`;
        log.info(`Connected to ${lastKnownServer}.`);
    });
    client.on('disconnected', (reason) => {
        if (reason) {
            if (lastKnownServer) {
                log.error(`Disconnected from ${lastKnownServer} (${reason})`);
            } else {
                log.error(`Disconnected (${reason})`);
            }
        } else {
            if (lastKnownServer) {
                log.error(`Disconnected from ${lastKnownServer}.`);
            } else {
                log.error(`Disconnected.`);
            }
        }
        process.exit(1);
    });
    client.on('join', (channel, user) => {
        if (normalise.user(user) !== options.identity.username) {
            return;
        }
        channel = normalise.channel(channel);
        log.info(`-> ${channel}`);
    });
    client.on('part', (channel, user) => {
        if (normalise.user(user) !== options.identity.username) {
            return;
        }
        channel = normalise.channel(channel);
        log.info(`<- ${channel}`);
    });

    log.info('IRC client created.');

    return client;
};
